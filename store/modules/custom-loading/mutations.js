import Constants from './constants'
import state from './state'

export default {

    [Constants.FETCH_LOADING_SHOW]: (state, payload) => {
        state.loadingShow = payload
        //뮤테이션 (돌연변이) 상태를 바꾸는거  스테이트의 값이 페이로딩 값으로 변경된다.
    }
}
