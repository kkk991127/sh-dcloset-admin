import customLoading from './modules/custom-loading'
import authenticated from './modules/authenticated'
import menu from './modules/menu'
import testData from './modules/test-data'
import goods from './modules/goods'
import member from './modules/member'
import notice from "@/store/modules/notice";
import delivery from './modules/delivery'


export const state = () => ({})

export const mutations = {}


export const modules = {

    customLoading,
    authenticated,
    menu,
    testData,
    goods,
    member,
    notice,
    delivery,



}
